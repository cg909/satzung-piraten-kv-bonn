Satzung des Kreisverbands Bonn
=============================

## §1 Name und Sitz ##

1. Die Piratenpartei Bonn ist ein Kreisverband (KV) der Bundespartei Piratenpartei Deutschland und des Landesverbandes Nordrhein-Westfalen.
   Dieser Kreisverband nennt sich im folgenden Piratenpartei Bonn.
2. Das Tätigkeitsgebiet der Piratenpartei Bonn ist die kreisfreie Stadt Bonn.
3. Sitz und Gerichtsstand ist die kreisfreie Stadt Bonn.

## §2 Mitgliedschaft ##

Die Mitgliedschaft, der Erwerb der Mitgliedschaft und die Beendigung der Mitgliedschaft werden durch die Landessatzung geregelt.

## §3 Organe und Öffentlichkeit ##

Organe der Piratenpartei Bonn sind:

1. Der Kreisparteitag (KPT).
2. Der Vorstand.
3. *Fachsprecher* sprechen für die Piratenpartei Bonn zu fachspezifischen Themen der Bonner Lokalpolitik.
5. Die Online-Mitgliederversammlung.

## §4 Der Kreisparteitag ##

1. Der Kreisparteitag (KPT) ist die Mitgliederversammlung der Piratenpartei Bonn.
   Der KPT tagt öffentlich.
   Die anwesenden Mitglieder können einen Ausschluss der Öffentlichkeit beschließen, wozu eine Zweidrittelmehrheit der abgegeben Stimmen notwendig ist.
2. Der KPT muss mindestens einmal jährlich einberufen werden und soll im zweiten Quartal durchgeführt werden
3. Die Einberufung des KPT erfolgt zwei Wochen vor dem angesetzten Termin durch Einladung an alle Mitglieder unter Angabe der Tagesordnungspunkte.
	1. Die Einladung erfolgt, sofern nicht anders beantragt, elektronisch, falls ein Mitglied offensichtlich nicht elektronisch erreicht werden kann wird dieses ohne Fristwahrung per Brief eingeladen.
	   Jedes Mitglied kann beantragen, seine Einladung schriftlich per Post zu bekommen.
	2. Der Vorstand ist verpflichtet, auf Antrag eines Zehntels der Mitglieder einen KPT einzuberufen.
	3. Für Aufstellungsversammlungen gilt eine verkürzte Einladungsfrist von einer Woche.
4. Kreisparteitage können grundsätzlich als Online-Kreisparteitage stattfinden.
   Auf Antrag eines einzelnen Mitgliedes muss allen Mitgliedern ein physischer Ort bekannt gemacht werden, an dem die Teilnahme am Parteitag möglich ist.
5. Zu den Aufgaben des KPT gehören:
	1. Die Wahl des Vorstands und seine Entlastung.
	2. Die Wahl aller Kandidaten und deren Vertreter für Wahlen zu politischen Gremien.
	3. Die Entscheidung zur Enthebung von Ämtern.
	4. Beschlussfassung über Satzung.
	5. Beschlussfassung über Wahl- und Grundsatzprogramme mit einfacher Mehrheit.
	6. Beschlussfassung zu Weisungen an den Vorstand und die Fachsprecher.
	7. Wahl eines Rechnungsprüfers findet sinngemäß Anwendung.
	8. Wahl eines Kassenprüfers findet sinngemäß Anwendung.
	9. Wahl der Fachsprecher.
6. Die Ergebnisse des KPT werden schriftlich festgehalten.
7. Antrags- und stimmberechtigt auf dem KPT sind alle Mitglieder und Nicht-Mitglieder.
   Mitglieder der Piratenpartei Bonn können bei einfacher  Mehrheit den Nicht-Mitgliedern das Antrags- und/oder das Stimmrecht
   entziehen.
8. Sämtliche Wahlverfahren werden durch die Geschäftsordnung des Kreisparteitags geregelt.

## §5 Der Vorstand ##

1. Der Vorstand ist dem KPT gegenüber rechenschaftspflichtig und weisungsgebunden.
   Der Vorstand tagt allgemein öffentlich.
   Er kann nach Begründung die Mitgliederöffentlichkeit herstellen.
   Ein Entsandter der Piraten Hochschulgruppen Bonn, hat immer Anwesenheits- und Rederecht in den Sitzungen des Vorstands.
   Ein Entsandter der jungen Piraten hat immer Anwesenheits- und Rederecht in den Sitzungen des Vorstands.
2. Er vertritt die Partei nach innen und außen und leistet Koordinierungsarbeit.
   In aktuellen politischen Fragen setzt der Vorstand seine und die Initiative der Mitglieder um, bis der KPT ihm durch ihre Beschlüsse jeweils Richtlinien gibt.
3. Der Vorstand besteht aus dem 1. Kreisvorsitzenden, dem 2. Kreisvorsitzenden, dem Schatzmeister, bis zu einem stellvertretenden Schatzmeister und bis zu vier Beisitzern.
   Die rechtsgeschäftliche Vertretung erfolgt durch zwei Vorstandsmitglieder, darunter mindestens ein Kreisvorsitzender oder der Schatzmeister.
4. Der 1. und 2. Kreisvorsitzende sind gleichberechtigt
5. Pflichten des Vorstands umfassen auch
	1. Der Vorstand soll bei Bundes- und Landesparteitagen und bei wichtigen Mumble-Konferenzen anwesend sein oder einen Vertreter benennen.
6. Der Vorstand ist beschlussfähig, wenn zumindest drei stimmberechtigte Mitglieder anwesend sind, und mindestens der 1. Vorsitzende, 2. Vorsitzende oder der Schatzmeister anwesend ist.
7. Der Schatzmeister ist berechtigt, gegen Ausgabenbeschlüsse, außerplanmäßige Ausgaben oder solche, die nicht durch entsprechende Einnahmen gedeckt sind, zu widersprechen.
   Diese Ausgaben dürfen dann nicht getätigt werden, es sei denn, der Vorstand lehnt mit 2/3 Mehrheit aller Stimmberechtigten den Widerspruch ab und stellt den Schatzmeister von der Verantwortung für diese Ausgabe frei.
8. Die Führung der Geschäftsstelle wird durch den Vorstand beauftragt und beaufsichtigt.
9. Tritt einer der Vorsitzenden zurück bzw. kann dieser seinen Aufgaben nicht mehr nachkommen, so wählt der Vorstand einen der Beisitzer für diese Position, der das Amt kommissarisch bis zum nächsten Kreisparteitag führt, welcher innerhalb von acht Wochen einzuberufen ist.
10. Tritt der Schatzmeister zurück bzw. kann dieser seinen Aufgaben nicht mehr nachkommen, so übernimmt der stellvertretende Schatzmeister diese Position kommissarisch bis zum nächsten Kreisparteitag.
    Ist das Amt desstellvertretenden Schatzmeisters nicht besetzt, wird Abs. 9 sinngemäß angewendet.
11. Tritt ein Beisitzer oder der stellvertretende Schatzmeister zurück oder kann seinen Aufgaben nicht mehr nachkommen, so wird sein Amt erst beim nächsten KPT neu besetzt.

### §5.1 Wahl des Vorstands ###

1. Der Vorstand wird von den am KPT teilnehmenden Mitgliedern des Kreisverbandes auf ein Jahr, vom Tage der Wahl an gerechnet, gewählt.
   Die Wahl wird schriftlich und geheim durchgeführt.
2. Jedes einzelne Mitglied des Vorstands oder auch der gesamte Vorstand kann jederzeit durch einen satzungsgemäß einberufenen KPT abgewählt werden.
3. Die Wahl findet nach Wahlordnung statt.
4. Nach Ende der Amtszeit bleibt der alte Vorstand bis zur satzungsgemäßen Neuwahl im Amt.

### §5.2 Geschäftsordnung ###

Der Vorstand gibt sich eine Geschäftsordnung und veröffentlicht diese angemessen.
Sie umfasst u.a. Regelungen zur:

1. Verwaltung der Mitgliedsdaten, deren Zugriff und Sicherung
2. Aufgaben und Kompetenzen der Vorstandsmitglieder
3. Dokumentation der Sitzungen
4. Abhaltung von Vorstandssitzungen, die auch virtuell oder fernmündlich stattfinden können
5. Form und Umfang des Tätigkeitsberichts
6. Beurkundung von Beschlüssen des Vorstandes

### §5.3 Initiativrecht ###

1. Jedes Mitglied kann den Vorstand zu einer Handlung auffordern, die sich im Aufgabenbereich des Vorstands befindet, insbesondere Stellungnahmen zu lokalpolitische Themen und Ereignissen oder Änderungen und Erweiterungen der Geschäftsordnung.
2. Jedes Mitglied kann jeden Fachsprecher zu einer Handlung auffordern, die sich im Aufgabenbereich des Fachsprechers befindet, insbesondere Stellungnahmen zu lokalpolitische Themen und Ereignissen.
3. Wird diese Aufforderung von 3% oder mehr Piraten unterstützt, so kann der Vorstand oder die Fachsprecher diese nur begründet abweisen.
4. Die Bearbeitung der Anträge müssen protokoliert werden und im Tätigkeitsbericht vollständig aufgenommen werden.

### §5.4 Misstrauensklausel ###

1. Fünf Mitglieder haben zusammen das Recht auf einem KPT ein Misstrauensvotum zu fordern.
   Dabei sind die selben Fristen wie für einen Satzungsänderungsantrag einzuhalten.

### §5.5 Handlungsunfähigkeit ###

Der Vorstand gilt als nicht handlungsfähig,

1. wenn mehr als 50% der im Kreisverband organisierten Piraten dem Vorstand das Misstrauen aussprechen, oder
2. wenn der Vorstand sich selbst für handlungsunfähig erklärt.

Tritt einer der vorgenannten Fälle ein, so ist innerhalb einer Frist von vier Wochen ein außerordentlicher KPT zur Wahl eines neuen Vorstandes einzuberufen.
Der verbliebene Vorstand kann einen kommissarischen Vorstand einsetzen, der jedoch nur mit der Vorbereitung des KPT beauftragt ist.
Tritt der gesamte Vorstand geschlossen zurück oder kann seinen Aufgaben nicht mehr nachkommen, so beruft er als letzte Amtshandlung einen ausserordentlichen Parteitag zur Neuwahl des Vorstandes ein.
Kommt er dieser Aufgabe nicht nach, beruft der Vorstand der nächst höheren Gliederung einen außerordentlichen Parteitag zur Neuwahl des Kreisvorstandes schnellstmöglich ein.

### §5.6 Rechenschaft ###

1. Der Vorstand publiziert mindestens alle vier Monate einen Tätigkeitsbericht.
   Dieser umfasst alle Tätigkeitsgebiete der Vorstandsmitglieder, wobei diese Berichte in Eigenverantwortung der Einzelnen / des Einzelnen erstellt werden.
   Dieser Bericht ist jedem Mitglied zugänglich zu machen.
2. Die Tätigkeitsberichte bilden eine der Grundlagen für die Entlastung des Vorstandes.
3. Tritt ein Vorstandsmitglied zurück, hat dieses unverzüglich einen Tätigkeitsbericht zu erstellen und dem Vorstand zuzuleiten.

## §6 Fachsprecher ##

1. Es kann zu jedem abgegrenzten Themengebiet der Kommunalpolitik Bonns einen Fachsprecher geben.
   Er kann im Namen der Piratenpartei Bonn zu den Diskussionen, Entscheidungen und Antragsentwürfen innerhalb seines Auschusses Stellung nehmen und Forderungen an diesen Ausschuss stellen.
2. Auch der Pressesprecher zählt zu den Fachsprechern.
3. Themengebiete werden von dem KPT oder dem Vorstand benannt und deren Umfang umrissen.
4. Die Fachsprecher
	1. werden vom Vorstand ernannt.
	   Sie müssen bei jedem KPT bestätigt werden.
	2. werden von des KPT gewählt.
	   Sie müssen bei jedem KPT bestätigt werden.
5. Die Tätigkeit des Fachsprechers kann vom Vorstand in einer Vorstandssitzung bis zur nächsten MV ausgesetzt werden.
   Die Entscheidung muss begründet werden.
6. Auf begründeten Antrag von drei Piraten muss der Vorstand über die Entlassung eines Fachsprechers debattieren und eine begründete Entscheidung fassen.
   Zur besagten Vorstandssitzung sollten die Antragsteller sowie der Fachsprecher präsent sein, um ihre Meinung kund zu tun.

## §7 Die Online-Mitgliederversammlung ##

1. Die Online-Mitgliederversammlung dient der Meinungsbildung und Beschlussfassung des Kreisverbands außerhalb von Parteitagen.
   Sie tagt nach der Konstituierung ständig.
2. Stimmberechtigt auf der Online-Mitgliederversammlung sind alle Mitglieder des Kreisverbands, die auch bei einem Kreisparteitag stimmberechtigt sind.
3. Die Online-Mitgliederversammlung kann
   1. Stellungnahmen,
   2. Positionspapiere sowie Änderungen an Positionspapieren,
   3. Wahlprogramme sowie Änderungen an Wahlprogrammen,
   4. Änderungen des Grundsatzprogramms und
   5. Sonstige Anträge, die nicht die Geschäftsordnung der Online-Mitgliederversammlung berühren,  
   beschließen.  
   Personenwahlen sind nicht zulässig.
4. Der Kreisparteitag beschließt die Geschäftsordnung der Online-Mitgliederversammlung, in der auch die Konstituierung der Online-Mitgliederversammlung geregelt ist.
5. Zwischen Einbringung und Ende der Abstimmung eines Antrags, der nicht ausschließlich eine Stellungnahme beinhaltet, müssen mindestens 24 Stunden liegen.
   Weitere Fristen regelt die Geschäftsordnung der Online-Mitgliederversammlung.
6. Auf Verlangen eines Mitglieds, das vor der Beschlussfassung eingebracht wurde, muss ein Beschluss der Online-Mitgliederversammlung auf dem nächsten Kreisparteitag bestätigt werden.
   Anträge die Parteiprogramme berühren müssen grundsätzlich von einem Kreisparteitag bestätigt werden.
7. Anträge und Beschlüsse der Online-Mitgliederversammlung werden unabhängig von (6) in das Protokoll des nächsten KPT eingefügt.

## §8 Ordnungsmaßnahmen ##

## §8 Ordnungsmaßnahmen ##

1. Alle Regelungen der Bundes- und Landessatzung zu Ordnungsmaßnahmen gelten entsprechend auch auf Kreisebene.
2. Stellt der Vorstand mittels einstimmigem Beschluss aller Vorstandsmitglieder eine wiederholte und fortgesetzte Störung der innerparteilichen Ordnung und Arbeit fest, kann er ein zeitlich begrenztes Hausverbot bis zu drei Monaten verhängen.
   Dieses betrifft sämtliche Arbeitstreffen und Kommunikationsmittel der Partei.
   Die Ausübung der Rechte auf Parteitagen und Mitgliederversammlungen bleibt davon unberührt.

## §9 Rechenschaftsbericht über Finanzen ##

1. Die Piratenpartei Bonn legt dem Landesverband NRW jährlich bis zum 31.03. Rechenschaft über ihr Vermögen, ihre Einnahmen und Ausgaben nach den Bestimmungen des § 24 des Parteiengesetzes ab.
2. Der Vorstand ist für die ordnungsgemäße Kassenführung der Piratenpartei Bonn verantwortlich und gewährleistet, dass die zum Erteilen eines Prüfungsvermerks für den Rechenschaftsbericht der Partei nach § 29 f PartG vorgeschriebenen Stichproben möglich sind.
3. Die Kassenprüfer kontrollieren die Kassenführung des Vorstands und legen dem KPT gegenüber Rechenschaft ab.
   Es ist mindestens vierteljährlich ein regelmässiger Finanzbericht des Schatzmeisters den Mitgliedern in Schriftform vorzulegen.
   Dieser muss ebenso von den Kassenprüfern geprüft werden.

## §10 Urabstimmung ##

In Satzungs- und Grundsatzfragen kann auf Beschluss des KPT oder auf Antrag eines Viertels der Mitglieder eine schriftliche Urabstimmung bei allen Mitgliedern durchgeführt werden.

## §11 Auflösung ##

1. Über die Auflösung der Piratenpartei Bonn entscheidet der satzungsgemäß einberufene KPT mit Zweidrittelmehrheit.

## §12 Ortsverbände ##

1. Ortsverbände können auf Initiative von mindestens sieben Mitgliedern gegründet werden.
2. Die Ortsverbände werden durch Beschluss des Kreisparteitags anerkannt.

## §13 Inkraftsetzung ##

Diese Satzung tritt am 10.04.2010 in Kraft.

## §14 Satzungsänderungen ##

1. Die Satzung kann nur durch Beschluss des KPT geändert werden, dieser muss den Wortlaut der Satzung ausdrücklich ändern oder ergänzen.
   Er bedarf der Zustimmung von zwei Dritteln der anwesenden Mitglieder des Kreisverbandes.
2. Änderungen der Satzung des Kreisverbandes kann der KPT nur beschließen, wenn sie auf der Tagesordnung der Einladung bekannt gegeben worden sind.
3. Satzungsänderungsanträge in Form von Dringlichkeitsanträgen sind unzulässig.
4. Redaktionelle Änderungen an Satzungsänderungsanträgen und Änderungen die den Geist eines Antrags nicht verändern bleiben auch auf einem KPT zulässig.

## §15 Programmänderungen ##

1. Die Programme des Kreisverbandes können nur durch Beschluss des KPT geändert werden.
   Dieser muss den Wortlaut ausdrücklich ändern oder ergänzen.
2. Programmanträge bedürfen der Zustimmung von zwei Dritteln der anwesenden Mitglieder des Kreisverbandes.
4. Redaktionelle Änderungen an Programmanträgen und Änderungen die den Geist eines Antrags nicht verändern bleiben auch auf einem KPT zulässig.

## §16 Verbindlichkeit dieser Satzung ##

Die Satzungen untergeordneter Gliederungen müssen mit den grundsätzlichen Regelungen dieser, sowie der Landes- und Bundessatzung übereinstimmen.

Finanzordnung der Piratenpartei Bonn
====================================

## § 1 Geschäftsjahr ##

1. Das Geschäftsjahr ist das Kalenderjahr.

## § 2 Mitgliedsbeitrag ##

1. Der Mitgliedsbeitrag wird durch die Bundessatzung geregelt.
2. Der Mitgliedsbeitrag ist an den Landesverband NRW zu entrichten, bzw. wird von diesem eingezogen.
3. Der Mitgliedsbeitrag wird vom Landesverband NRW aufgeteilt.
4. Es gilt die in der Satzung des Landesverbandes NRW aufgeführte Regelung zur Verteilung.

## § 3 Verzug und Mahnung ##

1. Es gelten die Regelungen des Bundesverbandes und des Landesverbandes NRW.

## § 4 Kassen- und Kontoführung ##

1. Eine Barkasse ist zu vermeiden.
2. Die Kassen- und Kontoführung erfolgt als geordnetes Belegwesen.
3. Der Kreisparteitag hat das Recht zur Kassen- und Kontoprüfung.
   Hierzu werden jährlich zwei oder mehr Kassenprüfer aus den Mitgliedern ausgewählt, die die Prüfung der Kasse für den Zeitraum seit der letzten Kassenprüfung vornehmen und der Hauptversammlung vor Entlastung des Vorstandes berichten.
4. Den Kassenprüfern sind ausnahmslos alle für die ordnungsgemäße Prüfung notwendigen Unterlagen vollständig vorzulegen.
   Der Schatzmeister hat den Kassenprüfern Rede und Antwort zu stehen.
5. Der Kreisparteitag kann beliebig über die Ausgabe der vorhandenen Mittel entscheiden.
6. Fasst der Kreisparteitag keinen gegenteiligen Beschluss, so verfügt der Vorstand über 60% der freien Mittel.
7. Der Kreisvorstand kann jederzeit Einsichtnahme in die Kassen- und Kontoführung beschließen.
   Der Schatzmeister hat im Falle eines solchen Beschlusses unverzüglich dem Kreisvorstand die Kassen- und Kontoführung offen zu legen.

## § 5 Rückstellungen ##

1. Es werden Rückstellungen für Kommunalwahlen gebildet.
   Dazu werden 1/5 aller ungebundenen Einnahmen dem Budget Kommunalwahlen zugeordnet.
   Zum 1.1. eines Kommunalwahljahres wird dieser Betrag für die Kommunalwahlen des Jahres zweckgebunden.
   Weitere ungebundene Einnahmen gelten ab dann bereits für darauffolgende Kommunalwahlen.
2. Es werden Rückstellungen für mögliche Rechtsstreitigkeiten vorgenommen.
3. Rückstellungen sind als Budgets im Haushaltsplan zu berücksichtigen.

## § 6 Jahresabschluss ##

1. Es ist ein Jahresabschluss des Kreisverbandes, sowie, durch den für Finanzangelegenheiten zuständigen Vorstandes, sowie aller untergeordneten Verbände, zu verfassen.
   Der Jahresabschluss umfasst Einnahmen, Ausgaben, Vermögenswerte sowie Anhängen und Erläuterungen und folgt den Vorschriften des Parteiengesetzes.
2. Die Jahresabschlüsse sind spätestens einen Monat nach Ende des Geschäftsjahres zu verfassen.
3. Jahresabschlüsse sind vom Vorsitzenden und dem Schatzmeister zu unterzeichnen.

## § 7 Aufbewahrungsfristen ##

1. Die Aufbewahrungsfrist für alle die Finanzangelegenheiten betreffenden Unterlagen, namentlich unter anderem Belege, Bücher, Jahresabschlüsse, beträgt 10 Jahre.
Die Frist beginnt mit Ablauf des Geschäftsjahres in dem die betreffenden Unterlagen verfasst wurden.

## § 8 Spenden ##

1. Es gelten die Regelungen des Bundesverbandes.

## § 9 Finanzierung ##

1. Es gelten die Regelungen des Bundesverbandes.

## § 10 Schlussbestimmungen ##

1. Alle nach der Finanzordnung geschehenden Tätigkeiten sind in elektronischer Form zu dokumentieren und nicht in Papierform, sofern dies rechtsgültig möglich ist.
2. Die Finanzordnung ist Teil der Satzung

